#!/bin/sh

set -x

__MESA_DIR="${CI_BUILDS_DIR}"/mesa
__PUSH=0
if [ "x$CI_PROJECT_NAMESPACE" == "xtanty" ] && [ "x$CI_COMMIT_REF_NAME" == "xmain" ] && [ "x$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME" != "xmain" ]; then
    __PUSH=1
fi

/usr/bin/wget -q -O- ${CI_SERVER_URL}/${FDO_UPSTREAM_REPO}/-/raw/${UI_MIRRORED_BRANCH}/.gitlab-ci/download-git-cache.sh | CI_PROJECT_DIR="${__MESA_DIR}" /bin/bash -

set -e

__UPSTREAM_REPOSITORY=${CI_SERVER_URL}/${FDO_UPSTREAM_REPO}.git
__FULL_MIRRORED_REPOSITORY="https://oauth2:${UI_MIRRORED_ACCESS_TOKEN}@${UI_MIRRORED_REPOSITORY}"

function clean() {
    set +e

    git remote remove upstream-mirrored-repository
    git remote remove mirrored-repository

    set -e
}

if [ -e "$__MESA_DIR"/.git ]; then
    cd "$__MESA_DIR"

    clean

    git remote add -t ${UI_MIRRORED_BRANCH} --no-tags mirrored-repository $__FULL_MIRRORED_REPOSITORY
else
    git clone -n -b ${UI_MIRRORED_BRANCH} -q --no-tags --depth 10 -o mirrored-repository $__FULL_MIRRORED_REPOSITORY "$__MESA_DIR"
    chmod a+w "$__MESA_DIR"
    cd "$__MESA_DIR"
fi

git remote add -f -t ${UI_MIRRORED_BRANCH} --no-tags upstream-mirrored-repository $__UPSTREAM_REPOSITORY
git checkout -B ${UI_MIRRORED_BRANCH} upstream-mirrored-repository/${UI_MIRRORED_BRANCH}

git pull --ff-only
if [ $__PUSH -eq 1 ]; then
    git push mirrored-repository ${UI_MIRRORED_BRANCH}
fi

clean
